# quantidade de cebola
cebolas = 300
# quantidade de cebola na caixa
cebolas_na_caixa = 120
# quantidade de cebolas que cabe por caixa
espaco_caixa = 5
# quantidade de caixa
caixas = 60


# cebolas que não está na caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
# quantidade de caixas que não tem cebola
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
# quantidade de caixas necessárias para pôr todas as cebolas que estão fora da caixa
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa


# vai dar o quantas cebolas estão na caixa
print ('Existem', cebolas_na_caixa, 'cebolas encaixotadas')
# vai dar o quantas cebolas estão fora da caixa
print ('Existem', cebolas_fora_da_caixa, 'cebolas sem caixa')
# vai dar a quantidade de cebolas que cabe por caixa
print ('Em cada caixa cabem', espaco_caixa, 'cebolas')
# vai dar a quantidade de caixa vazia
print ('Ainda temos', caixas_vazias, 'caixas vazias')
# vai dar a quantidade que falta de caixa para por todas as cebolas
print ('Então, precisamos de', caixas_necessarias, 'caixas para empacotar todas as cebolas')